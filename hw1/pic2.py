import cv2
import numpy as np

image = cv2.imread('pic2.jpg')

gray_pic2 = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#cv2.imshow('gray_pic2.jpg', gray_pic2)
cv2.imwrite('gray_pic2.jpg', gray_pic2)

filter_pic2 = cv2.GaussianBlur(gray_pic2, (9,9),0)
#cv2.imshow('filter_pic2.jpg', filter_pic2)
cv2.imwrite('filter_pic2.jpg', filter_pic2)

ret, binarization_pic2 = cv2.threshold(filter_pic2, 127, 255, cv2.THRESH_OTSU)
#cv2.imshow('binarization_pic2.jpg', binarization_pic2)
cv2.imwrite('binarization_pic2.jpg', binarization_pic2)

kernel = np.ones((5,5),np.uint8)
morphology_pic2 = cv2.morphologyEx(binarization_pic2, cv2.MORPH_OPEN, kernel,iterations=3)

#cv2.imshow('morphology_pic2.jpg', morphology_pic2)
cv2.imwrite('morphology_pic2.jpg', morphology_pic2)

cv2.waitKey(0)