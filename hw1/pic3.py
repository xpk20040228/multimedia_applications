import cv2
import numpy as np

image = cv2.imread('pic3.jpg')

gray_pic3 = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#cv2.imshow('gray_pic3.jpg', gray_pic3)
cv2.imwrite('gray_pic3.jpg', gray_pic3)

filter_pic3 = cv2.GaussianBlur(gray_pic3, (5, 5), 0)
#cv2.imshow('filter_pic3.jpg', filter_pic3)
cv2.imwrite('filter_pic3.jpg', filter_pic3)

ret, binarization_pic3 = cv2.threshold(filter_pic3, 135, 255, cv2.THRESH_BINARY)
#cv2.imshow('binarization_pic3.jpg', binarization_pic3)
cv2.imwrite('binarization_pic3.jpg', binarization_pic3)

kernel = np.ones((5,5),np.uint8)
morphology_pic3 = cv2.morphologyEx(binarization_pic3, cv2.MORPH_OPEN, kernel,iterations=3)
#cv2.imshow('morphology_pic3.jpg', morphology_pic3)
cv2.imwrite('morphology_pic3.jpg', morphology_pic3)

cv2.waitKey(0)