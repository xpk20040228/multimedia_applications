import cv2

# Load the image
image = cv2.imread('people.jpg')

# Define the coordinates of the rectangle
top_left = (170, 260)  # Change to the top-left corner of the face
bottom_right = (400, 560)  # Change to the bottom-right corner of the face

label = 'Person'
position = (170, 250)

# Draw the rectangle on the image
cv2.rectangle(
    img=image,
    pt1=top_left,
    pt2=bottom_right,
    color=(255, 0, 0),  # Rectangle color in BGR
    thickness=2  # Line thickness
)

cv2.putText(
    img=image,
    text=label,
    org=position,
    fontFace=cv2.FONT_HERSHEY_SIMPLEX,
    fontScale=2,  # Font scale
    color=(255, 0, 0),  # Font color in BGR
    thickness=3  # Line thickness
)

# Save the result
cv2.imwrite('label_people.jpg', image)
