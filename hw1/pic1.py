import cv2
import numpy as np

image = cv2.imread('pic1.jpg')

gray_pic1 = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
#cv2.imshow('gray_pic1.jpg', gray_pic1)
cv2.imwrite('gray_pic1.jpg', gray_pic1)

filter_pic1 = cv2.medianBlur(gray_pic1, 5)
#cv2.imshow('filter_pic1.jpg', filter_pic1)
cv2.imwrite('filter_pic1.jpg', filter_pic1)

ret, binarization_pic1 = cv2.threshold(filter_pic1, 85, 255, cv2.THRESH_BINARY)
#cv2.imshow('binarization_pic1.jpg', binarization_pic1)
cv2.imwrite('binarization_pic1.jpg', binarization_pic1)

kernel = np.ones((3,3),np.uint8)
morphology_pic1 = cv2.morphologyEx(binarization_pic1, cv2.MORPH_GRADIENT, kernel,iterations=3)
#cv2.imshow('morphology_pic1.jpg', morphology_pic1)
cv2.imwrite('morphology_pic1.jpg', morphology_pic1)

cv2.waitKey(0)