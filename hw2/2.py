import cv2
import numpy as np

image = cv2.imread('2.jpg')

gray_2 = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
cv2.imshow('gray_2.jpg', gray_2)
cv2.imwrite('gray_2.jpg', gray_2)

filter_2 = cv2.medianBlur(gray_2, 11)
cv2.imshow('filter_2.jpg', filter_2)
cv2.imwrite('filter_2.jpg', filter_2)

edge_2 = cv2.Canny(filter_2, 255/3, 230)
cv2.imshow('edge_2.jpg', edge_2)
cv2.imwrite('edge_2.jpg', edge_2)

kernel = np.ones((17,17),np.uint8)
morphology_2 = cv2.morphologyEx(edge_2, cv2.MORPH_CLOSE, kernel,iterations=3)
cv2.imshow('morphology_2.jpg', morphology_2)
cv2.imwrite('morphology_2.jpg', morphology_2)

lines_2 = cv2.HoughLinesP(morphology_2, 1.0, np.pi / 180, 270, 500, 500)

lines_img = np.zeros_like(image)

if lines_2 is not None:
    for line in lines_2:
        x1, y1, x2, y2 = line[0]
        cv2.line(lines_img, (x1, y1), (x2, y2), (255, 0, 0), 2)

cv2.imshow('lines_2.jpg', lines_img)
cv2.imwrite('lines_2.jpg', lines_img)

result_2 = image.copy();
if lines_2 is not None:
    for line in lines_2:
        x1, y1, x2, y2 = line[0]
        cv2.line(result_2, (x1, y1), (x2, y2), (255, 0, 0), 2)
cv2.imshow('result_2.jpg', result_2)
cv2.imwrite('result_2.jpg', result_2)

cv2.waitKey(0)
cv2.destroyAllWindows()