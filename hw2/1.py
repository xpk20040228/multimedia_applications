import cv2
import numpy as np

image = cv2.imread('1.jpg')

gray_1 = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
cv2.imshow('gray_1.jpg', gray_1)
cv2.imwrite('gray_1.jpg', gray_1)

filter_1 = cv2.medianBlur(gray_1, 11)
cv2.imshow('filter_1.jpg', filter_1)
cv2.imwrite('filter_1.jpg', filter_1)

edge_1 = cv2.Canny(filter_1, 100, 200)
cv2.imshow('edge_1.jpg', edge_1)
cv2.imwrite('edge_1.jpg', edge_1)

kernel = np.ones((3,3),np.uint8)
morphology_1 = cv2.morphologyEx(edge_1, cv2.MORPH_CLOSE, kernel,iterations=3)
cv2.imshow('morphology_1.jpg', morphology_1)
cv2.imwrite('morphology_1.jpg', morphology_1)

lines_1 = cv2.HoughLinesP(morphology_1, 1.0, np.pi / 180, 145, 200, 50)
lines_img = np.zeros_like(image)

if lines_1 is not None:
    for line in lines_1:
        x1, y1, x2, y2 = line[0]
        cv2.line(lines_img, (x1, y1), (x2, y2), (255, 0, 0), 2)

cv2.imshow('lines_1.jpg', lines_img)
cv2.imwrite('lines_1.jpg', lines_img)

result_1 = image.copy();
if lines_1 is not None:
    for line in lines_1:
        x1, y1, x2, y2 = line[0]
        cv2.line(result_1, (x1, y1), (x2, y2), (255, 0, 0), 2)
cv2.imshow('result_1.jpg', result_1)
cv2.imwrite('result_1.jpg', result_1)

cv2.waitKey(0)
cv2.destroyAllWindows()