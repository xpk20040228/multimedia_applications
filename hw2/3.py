import cv2
import numpy as np

image = cv2.imread('3.jpg')

gray_3 = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
cv2.imshow('gray_3.jpg', gray_3)
cv2.imwrite('gray_3.jpg', gray_3)

filter_3 = cv2.medianBlur(gray_3, 7)
cv2.imshow('filter_3.jpg', filter_3)
cv2.imwrite('filter_3.jpg', filter_3)

edge_3 = cv2.Canny(filter_3, 170, 255)
cv2.imshow('edge_3.jpg', edge_3)
cv2.imwrite('edge_3.jpg', edge_3)

kernel = np.ones((3,3),np.uint8)
morphology_3 = cv2.morphologyEx(edge_3, cv2.MORPH_CLOSE, kernel,iterations=3)
cv2.imshow('morphology_3.jpg', morphology_3)
cv2.imwrite('morphology_3.jpg', morphology_3)

lines_3 = cv2.HoughLinesP(morphology_3, 1.0, np.pi / 180, 50, 200, 75)
lines_img = np.zeros_like(image)

if lines_3 is not None:
    for line in lines_3:
        x1, y1, x2, y2 = line[0]
        cv2.line(lines_img, (x1, y1), (x2, y2), (255, 0, 0), 2)

cv2.imshow('lines_3.jpg', lines_img)
cv2.imwrite('lines_3.jpg', lines_img)

result_3 = image.copy();
if lines_3 is not None:
    for line in lines_3:
        x1, y1, x2, y2 = line[0]
        cv2.line(result_3, (x1, y1), (x2, y2), (255, 0, 0), 2)
cv2.imshow('result_3.jpg', result_3)
cv2.imwrite('result_3.jpg', result_3)

cv2.waitKey(0)
cv2.destroyAllWindows()